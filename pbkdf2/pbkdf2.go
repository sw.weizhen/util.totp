package pbkdf2

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

// https://opensource.apple.com/source/Security/Security-54.1.3/AppleCSP/PBKDF2/pbkdf2.c
func derivedKeySHA256(password, salt []byte, iterate int) []byte {

	sha256 := hmac.New(sha256.New, password)
	sha256Size := sha256.Size()

	block := (cst_PBKDF2_DK_LEN + sha256Size - 1) / sha256Size

	var buf [4]byte
	dk := make([]byte, 0, block*sha256Size)
	u := make([]byte, sha256Size)

	for i := 1; i <= block; i++ {
		sha256.Reset()
		sha256.Write(salt)

		buf[0] = byte(i >> 24)
		buf[1] = byte(i >> 16)
		buf[2] = byte(i >> 8)
		buf[3] = byte(i)

		sha256.Write(buf[:4])

		dk = sha256.Sum(dk)
		t := dk[len(dk)-sha256Size:]
		copy(u, t)

		for i := 2; i <= iterate; i++ {
			sha256.Reset()
			sha256.Write(u)

			u = u[:0]
			u = sha256.Sum(u)
			for x := range u {
				t[x] ^= u[x]
			}
		}
	}

	return dk[:cst_PBKDF2_DK_LEN]
}

func generateSalt() string {

	// salt := make([]byte, cst_PBKDF2_SALT_LEN)
	// rand.Read(salt)
	// for i := 0; i < cst_PBKDF2_SALT_LEN; i++ {
	// 	salt[i] = cst_PBKDF2_SALT_CHAR[salt[i]%byte(len(cst_PBKDF2_SALT_CHAR))]
	// }

	rand.Seed(time.Now().UnixNano())

	var sb strings.Builder
	for i := 0; i < cst_PBKDF2_SALT_LEN; i++ {
		sb.WriteRune([]rune(cst_PBKDF2_SALT_CHAR)[rand.Intn(len(cst_PBKDF2_SALT_CHAR))])
	}

	return sb.String()
}

func GeneratePBKDF2Hash(password string) string {
	salt := generateSalt()

	pbkdf2 := derivedKeySHA256([]byte(password), []byte(salt), cst_PBKDF2_ITERATE)
	hash := hex.EncodeToString(pbkdf2)

	return fmt.Sprintf(cst_PBKDF2_FORMAT, cst_PBKDF2_ITERATE, salt, hash)
}

func VerifyPBKDF2Hash(password, pbkdf2Hash string) (bool, error) {
	sp := strings.Split(pbkdf2Hash, cst_SIGN_DOLLAR)
	if len(sp) != 3 {
		return false, fmt.Errorf("unexpected pbkdf2Hash format -> %s", pbkdf2Hash)
	}

	salt := sp[1]
	method := strings.Split(sp[0], cst_SIGN_COLON)
	if len(method) != 3 {
		return false, fmt.Errorf("unexpected method format -> %s", sp[0])
	}

	iterate, err := strconv.Atoi(method[2])
	if err != nil {
		return false, err
	}

	exceptPBDKF2Hash := hex.EncodeToString(derivedKeySHA256([]byte(password), []byte(salt), iterate))

	return strings.EqualFold(sp[2], string(exceptPBDKF2Hash)), nil
}
