package pbkdf2

const (
	cst_PBKDF2_SALT_CHAR = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	cst_PBKDF2_FORMAT    = "pbkdf2:sha256:%d$%s$%s" // pbkdf2:ALGO:ITERATION:SALT:DK
)

const (
	cst_PBKDF2_SALT_LEN = 8
	cst_PBKDF2_DK_LEN   = 32
	cst_PBKDF2_ITERATE  = 150000
)

const (
	cst_SIGN_DOLLAR = "$"
	cst_SIGN_COLON  = ":"
)
