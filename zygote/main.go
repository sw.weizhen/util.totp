package main

import (
	"fmt"

	"io/ioutil"
	"time"

	totp "gitlab.com/sw.weizhen/util.totp"
	pbkdf2 "gitlab.com/sw.weizhen/util.totp/pbkdf2"
)

func main() {
	fmt.Println("sw.totp::main()")

	optauth, _ := totp.New(
		totp.TotpOpts{
			Account: "Gillain",
			Expire:  60 * 5,
			HMAC:    totp.CST_HMAC_SHA512,
			CodeLen: totp.CST_CODE_LEN_8,
		},
	)

	fmt.Println(optauth)
	fmt.Printf("secret: %v\n", optauth.Secret())
	fmt.Printf("opt rul: %v\n", optauth.String())

	qrcode, _ := totp.QRCode(optauth.String(), 500)

	ioutil.WriteFile("aaa.png", qrcode, 0644)

	for {
		code, err := totp.Code(optauth.Secret(), 60*5, totp.CST_CODE_LEN_8, totp.CST_HMAC_SHA512)
		if err != nil {
			fmt.Printf("code error: %v\n", code)
			return
		}

		pbkdf2Hash := pbkdf2.GeneratePBKDF2Hash(code)
		fmt.Printf("time: %v, code: %s, pbkdf2: %v\n", time.Now(), code, pbkdf2Hash)
		fmt.Println(pbkdf2.VerifyPBKDF2Hash(code, pbkdf2Hash))

		time.Sleep(time.Second * 3)
	}

}
