package swtotp

import (
	"net/url"
	"strings"
)

type OTPAuth struct {
	urlKey *url.URL
}

func (ref *OTPAuth) Secret() string {
	return ref.urlKey.Query().Get(cst_K_SECRET)
}

func (ref *OTPAuth) String() string {
	return strings.TrimSpace(ref.urlKey.String())
}
