package swtotp

import (
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"hash"
)

type hmacAlgo int

func (ref hmacAlgo) String() string {
	switch ref {
	case CST_HMAC_SHA1:
		return cst_DES_SHA1

	case CST_HMAC_SHA256:
		return cst_DES_SHA256

	case CST_HMAC_SHA512:
		return cst_DES_SHA512

	default:
		return cst_DES_SHA1
	}
}

func (ref hmacAlgo) Hash() hash.Hash {
	switch ref {
	case CST_HMAC_SHA1:
		return sha1.New()

	case CST_HMAC_SHA256:
		return sha256.New()

	case CST_HMAC_SHA512:
		return sha512.New()

	default:
		return sha1.New()
	}
}
